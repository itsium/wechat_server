// ### Lib import
var express = require('express'),
	morgan  = require('morgan'),
	bodyParser = require('body-parser'),
	sha1  = require('sha1'),
	xml = require('xml'),
	xmlparser = require('express-xml-bodyparser'),
	exphbs  = require('express3-handlebars'),
	js2xmlparser = require("js2xmlparser"),
	path = require("path");


var sitemap = require("./sitemap.json");
var app = express();

// ### Middlewares
app.use(morgan('combined'));
app.use(express.json());
app.use(express.urlencoded());
app.use(xmlparser());
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(application_root, "public")));
app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

// ### Global vars
var token = "popopopopo";
var userID = "gh_cdc92d92b52d";
var appID = "wx7c35f10103e31584";
var appSecret = "da3cd9ba13eaf82a1dfd9ed83d6664bc";
var js2xmlparserOptions = {
    		useCDATA: true
};


var JSONParser = {
	encode: function(data) {
		return JSON.stringify(data);
	},
	decode: function(data) {
		return JSON.parse(data);
	}
};

// ### Fonction to verify request signature
function verifSignature(query) {
	// Sort params
	var params = [token, query.timestamp, query.nonce];
	params.sort();
	// Generate the sha1 of param string
	var hash = sha1(params.join(""));
	if(hash == query.signature) {
		return true;
	} else {
		return false;
	}
}

// // ### Function to get the access_token
// function getAccessToken()

function xmlResponse(res, datajson) {
	res.set('Content-Type', 'text/xml');
	res.send(js2xmlparser("xml", datajson, js2xmlparserOptions));
}

function jsonResponse(res, datajson) {
	res.set('Content-Type', 'text/json');
	res.send(JSONParser.encode(datajson));
}

function textMessage(req, res, text) {
	xmldata = req.body.xml;
	if (req.body.xml.content == "follow") {
		var datajson = {
			ToUserName: xmldata.fromusername[0],
			FromUserName: userID,
			CreateTime: Date.now(),
			MsgType: "text",
			Content: "<a href='http://wpp.itsium.cn/follow'>click here</a><a href='http://wpp.itsium.cn/all'>ALL FUnc</a>"
		};
	} else {
		var datajson = {
			ToUserName: xmldata.fromusername[0],
			FromUserName: userID,
			CreateTime: Date.now(),
			MsgType: "text",
			Content: text
		};
	}

	xmlResponse(res, datajson);
}

function voiceMessage(req, res, mediaid) {
	xmldata = req.body.xml;
	var datajson = {
			ToUserName: xmldata.fromusername[0],
			FromUserName: userID,
			CreateTime: Date.now(),
			MsgType: "voice",
			Voice : { MediaId: xmldata.mediaid }
		};
	xmlResponse(res, datajson);
}



function answerMachine(req, res) {
	switch(req.body.xml.msgtype[0]) {
		case 'text' :
		  textMessage(req, res, "popo");
		  break;
		case 'voice' :
		  voiceMessage(req, res, "popo");
		  break;
		default:
		  console.log("[UNHANDLED MESSAGE]  type:" + req.body.xml.msgtype + "\n" , req.body);
		  res.send("");
	}
}


app.get('/hello.json', function(request, response) {
	response.set('Content-Type', 'application/json');
	response.send(JSONParser.encode({hello: 'world'}));
});

app.get('/follow', function(req, res) {
	res.render('follow');
});
app.get('/all', function(req, res) {
	res.render('all');
});

app.get('/', function(req, res){
	if (verifSignature(req.query)) {
		res.send(req.query.echostr);
	} else {
		res.send("KO");
		// textMessage(req, res, "popo");
	}
  	console.log("[QUERY] : " , req.query);
  	console.log("[BODY] : ", req.body);
});

app.post('/', function(req, res){

	if (verifSignature(req.query)) {
		answerMachine(req, res)
	} else {
		res.send("KO");
	}
  	console.log("[QUERY] : " , req.query);
  	console.log("[BODY] : ",  req.body);
});

var server = app.listen((process.env.PORT || 3000), function() {
    console.log('Listening on port %d', server.address().port);
});

