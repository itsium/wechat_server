#!/usr/bin/python

from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import xml.etree.ElementTree as ET

PORT_NUMBER = 8080

class JSONParser(object):

	@staticmethod
	def decode(data):
		try:
			return json.loads(data)
		except Exception as e:
			print repr(e)
			return False

	@staticmethod
	def encode(data):
		try:
			return json.dumps(data)
		except Exception as e:
			print repr(e)
			return False

class XMLParser(object):

	@staticmethod
	def decode(data):
		try:
			return ET.fromstring(data)
		except Exception as e:
			print repr(e)
			return False

	# http://effbot.org/zone/element-index.htm
	@staticmethod
	def encode(data):
		feed = etree.Element('{http://www.w3.org/2005/Atom}feed',
			attrib={'{http://www.w3.org/XML/1998/namespace}lang': 'en'})
		return etree.tostring(feed)

#This class will handles any incoming request from
#the browser
class myHandler(BaseHTTPRequestHandler):

	#Handler for the GET requests
	def do_GET(self):
		self.send_response(200)
		self.send_header('Content-type', 'text/html')
		self.end_headers()
		# Send the html message
		self.wfile.write("Hello World !")
		return


if __name__ == '__main__':

	try:
		#Create a web server and define the handler to manage the
		#incoming request
		server = HTTPServer(('', PORT_NUMBER), myHandler)
		print 'Started httpserver on port ' , PORT_NUMBER

		#Wait forever for incoming htto requests
		server.serve_forever()

	except KeyboardInterrupt:
		print '^C received, shutting down the web server'
		server.socket.close()
